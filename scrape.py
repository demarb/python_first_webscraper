#pip installed these packages
# - beautifulsoup4
# - lxml      (html5lib is an alternative)
# - requests

# Importing packages

from bs4 import BeautifulSoup
import requests


# Passing html to beautiful soup

with open('index.html') as html_file:
    soup = BeautifulSoup(html_file, 'lxml')


# print(soup.prettify())

# match = soup.title
# print(match)

# match = soup.title.text
# print(match)

# match = soup.div
# print(match)

# match = soup.div
# print(match)

# match = soup.find('div')
# print(match)

for article in soup.find_all('div', class_='article'):
    # print(article)

    headline = article.h2.a.text
    print(headline)

    summary = article.p.text
    print(summary)

    print()